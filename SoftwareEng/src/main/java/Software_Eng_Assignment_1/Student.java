package Software_Eng_Assignment_1;

public class Student {

	private String FirstName;
	private String LastName;
	private int age;
	private String DOB;
	private int ID;

	public Student(String FirstName, String LastName, int age, String DOB, int ID) {
		this.setFirstName(FirstName);
		this.setLastName(LastName);
		this.age = age;
		this.DOB = DOB;
		this.ID = ID;

	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getUserName() {
		return FirstName + age;
	}

	public void setUserName(String userName) {
		userName = FirstName + age;
	}

	public String toString() {
		return FirstName + "  " + LastName + "\n"+"UserName: " +getUserName();

	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

}
