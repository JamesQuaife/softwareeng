package Software_Eng_Assignment_1;

public class CollegeTest {
	
	

	public static void main(String[] args) {
		
		String output = "";
		
		courseProgram courseProgram1 = new courseProgram("CSIT","08/09/2017","25/05/2018");
		courseProgram courseProgram2 = new courseProgram("ECE","08/10/2017","23/05/2018");
		
		
		
		Module module1 = new Module("cryptography","CS402");
		Module module2 = new Module("networks","CS4423");
		Module module3 = new Module("realTimeSystems","CT420");
		Module module4 = new Module("AI","CT421");
		
		courseProgram1.add(module1);
		courseProgram1.add(module2);
		courseProgram1.add(module3);
		courseProgram1.add(module4);
		
		Student student1 = new Student("James","Quaife",37,"06/10/1981",14100104);
		Student student2 = new Student("John","Quaife",35,"06/02/1984",15001058);
		Student student3 = new Student("Jake","Quaife",33,"37/02/1986",15005059);
		
		module1.add(student1);
		module2.add(student2);
		module3.add(student3);
		
		if (student1 != null) {
			try {
				output += student1.toString() + "\nStudent ID:" + student1.getID() + "\nUserName: "
						+ student1.getUserName() + "\nModuleName:" + module1.getModuleName() + "\nModuleID:"
						+ module1.getModuleID() + "\nCourseProgram:" + courseProgram1.getCourseName() + "\n"
						+ "Start Date: " + courseProgram1.getStartDate() + "\n" + "End Date: "
						+ courseProgram1.getEndDate() + "\n";
			} catch (Exception e) {

			}
		}

		if (student2 != null) {
			try {
				output += "\n" + student2.toString() + "\nStudent ID:" + student2.getID() + "\nUserName: "
						+ student2.getUserName() + "\nModuleName:" + module2.getModuleName() + "\nModuleID:"
						+ module2.getModuleID() + "\nCourseProgram:" + courseProgram1.getCourseName() + "\n"
						+ "Start Date: " + courseProgram1.getStartDate() + "\n" + "End Date: "
						+ courseProgram1.getEndDate() + "\n";
				;
			} catch (Exception e) {

			}
		}
		if (student3 != null) {
			try {
				output += "\n" + student3.toString() + "\nStudent ID:" + student3.getID() + "\nUserName: "
						+ student3.getUserName() + "\nModuleName:" + module2.getModuleName() + "\nModuleID:"
						+ module2.getModuleID() + "\nCourseProgram:" + courseProgram2.getCourseName() + "\n"
						+ "Start Date: " + courseProgram2.getStartDate() + "\n" + "End Date: "
						+ courseProgram2.getEndDate() + "\n";
				;
			} catch (Exception e) {

			}  
		}
		
	
		System.out.println(output);
		
	
		
		
	}	
	

}
