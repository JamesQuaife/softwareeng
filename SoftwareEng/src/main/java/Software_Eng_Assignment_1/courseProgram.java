package Software_Eng_Assignment_1;

import java.util.ArrayList;

public class courseProgram {
	
	private courseProgram program;
	
	public void add(courseProgram program) {
		this.setProgram(program);
		
	}

	private String courseName;
	private ArrayList<Module> modules = new ArrayList<Module>();

	private String startDate;
	private String endDate;
	
	public courseProgram(String courseName,String startDate, String endDate) {
		this.courseName = courseName;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	public void add(Module module){
		modules.add(module);
	}
	

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public String toString(){
		return "CourseName:" +courseName+", "+"Start Date: "+startDate+", "+"End Date: "+endDate+" ";
	}

	public courseProgram getProgram() {
		return program;
	}

	public void setProgram(courseProgram program) {
		this.program = program;
	}

}
