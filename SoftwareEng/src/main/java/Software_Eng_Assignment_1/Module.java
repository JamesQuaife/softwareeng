package Software_Eng_Assignment_1;

import java.util.ArrayList;

public class Module {

	private String moduleName;
	private String moduleID;
	private ArrayList<Student> students = new ArrayList<Student>();


	public Module(String moduleName, String moduleID) {
		this.moduleName = moduleName;
		this.moduleID = moduleID;
	}

	public void add(Student student) {
		students.add(student);
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleID() {
		return moduleID;
	}

	public void setModuleID(String moduleID) {
		this.moduleID = moduleID; 
	}

	public String toString() {
		return "Module Name: " + moduleName + ", " + "ModuleID: " + moduleID + " ";
	}

}
